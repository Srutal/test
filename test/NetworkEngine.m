//
// Created by srutka on 12/07/16.
// Copyright (c) 2016 srutka. All rights reserved.
//

#import "NetworkEngine.h"

@implementation NetworkEngine

- (instancetype)init {
    self = [super init];
    if (self) {

        self.manager = [AFHTTPSessionManager manager];
        [self.manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        [self.manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
        [self.manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [self.manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];

    }

    return self;
}

+ (NSDictionary *)toJSONRequest:(NSString *)method parametr:(NSDictionary *)param {

    NSMutableArray *params = [[NSMutableArray alloc]init];
    [params addObject:param];

    NSMutableDictionary * ret = [[NSMutableDictionary alloc] init];

    ret[@"method"] = method;
    ret[@"params"] = params ?: @"";

    return ret;
}

- (NSURLSessionDataTask *)sendRequest:(NSDictionary *)request onCompletion:(void (^)(NSDictionary *posts, NSError *error))block {

    if ([request[@"method"] isEqualToString:@"putImage"]) {
        _url = @"http://31.31.77.58:8091/iostest/UserData";
    } else {
        _url = @"http://31.31.77.58:8091/iostest/Login";
    }

    return [self.manager POST:_url parameters:request progress:nil success:^(NSURLSessionDataTask * __unused task, id JSON) {

        NSError *err = JSON[@"error"][@"code"];

        if (block) {
            block(JSON,err);
        }
    } failure:^(NSURLSessionDataTask * __unused task, NSError *error) {
        if (block) {
            block(nil, error);
        }
    }];
}

@end