//
// Created by srutka on 13/07/16.
// Copyright (c) 2016 srutka. All rights reserved.
//

#import "ComLayer.h"
#import "NetworkEngine.h"
#import "Units.h"
#import "UserInfo.h"
#import "AppData.h"
#import "LocalAuth.h"

@implementation ComLayer {
    NetworkEngine *_engine;
}

@synthesize engine = _engine;

- (instancetype)init {
    self = [super init];
    if (self) {
        self.engine = [[NetworkEngine alloc] init];
    }

    return self;
}

- (void)doRegistrationWith:(NSString *)email password:(NSString *)password onCompletion:(void (^)(NSString *))completion {

    NSDictionary *params = @{@"name": email};

    NSDictionary *request = [NetworkEngine toJSONRequest:@"generateSalt" parametr:params];

    [_engine sendRequest:request onCompletion:^(NSDictionary *result, NSError *error) {
        if (!error) {

            NSString *hash = [NSString stringWithFormat:@"%@%@", password, result[@"result"][@"salt"]];

            NSString *passwordHashed = [Units sha1:hash];
            NSLog(@"passwordHashed %@", passwordHashed );

            NSDictionary *params = @{@"name": email, @"passwordHashed": passwordHashed};

            NSDictionary *request = [NetworkEngine toJSONRequest:@"registerUser" parametr:params];

            [_engine sendRequest:request onCompletion:^(NSDictionary *result, NSError *error) {
                if (!error) {

                    aUser = [[UserInfo alloc] initWithDictionary:result[@"result"]];

                    completion([NSString stringWithFormat:@"Registrovan"]);
                    [self doLoginWith:email password:password enableAutologin:false onCompletion:^(BOOL complet) {
                        completion(complet? @"Po reg Prihlasen":@"Po reg Neprihlasen");
                    }];

                } else {
                    NSLog(@"ses kun.. %@", error);
                    completion([NSString stringWithFormat:@"Chyba %@",error]);
                }
            }];

        } else {
            NSLog(@"ses kun.. %@", error);
            completion([NSString stringWithFormat:@"Chyba %@",error]);
        }
    }];
}

- (void)doLoginWith:(NSString *)email password:(NSString *)password enableAutologin:(bool)enableAutologin onCompletion:(void (^)(BOOL))completion {

    NSDictionary *params = @{@"name": email};

    NSDictionary *request = [NetworkEngine toJSONRequest:@"readChallenge" parametr:params];

    [_engine sendRequest:request onCompletion:^(NSDictionary *result, NSError *error) {
        if (!error) {

            NSString *challenge = result[@"result"][@"challenge"];
            NSString *saltX = [NSString stringWithFormat:@"%@%@", password, result[@"result"][@"salt"]];

            NSString *hash1 = [NSString stringWithFormat:@"%@%@", [Units sha1:saltX], challenge];
            NSString *hash2 = [Units sha1:hash1];

            NSDictionary *params = @{@"name": email, @"passwordHashed": hash2};

            NSDictionary *request = [NetworkEngine toJSONRequest:@"login" parametr:params];

            [_engine sendRequest:request onCompletion:^(NSDictionary *result, NSError *error) {
                if (!error) {

                    aUser = [[UserInfo alloc] initWithDictionary:result[@"result"]];

                    // pokud si chceme pamatovat udaje
                    if(enableAutologin) {
                        [Units storeAuth:[LocalAuth authWithLogin:email password:password]];
                    }

                    completion(true);

                } else {
                    NSLog(@"ses kun.. %@",error);
                    [Units storeAuth:nil];
                    aUser = nil;
                    completion(false);
                }
            }];

        } else {
            NSLog(@"ses kun.. %@",error);
            completion(false);
        }
    }];
}

- (void)loginWithStoredAuth:(void (^)(BOOL))completion {

        LocalAuth *auth = [Units loadAuth];
        if (auth == nil){
            NSLog(@"No stored auth info");
        }

    [self doLoginWith:auth.email password:auth.password enableAutologin:true onCompletion:completion];
}

+ (bool)hasAutologinData {
    return [Units loadAuth] != nil;
}

- (void)uploadImage:(NSString *)imageString completion:(void (^)(BOOL))success {

    NSDictionary *params = @{@"fileContent": imageString};

    NSDictionary *request = [NetworkEngine toJSONRequest:@"putImage" parametr:params];

    [_engine sendRequest:request onCompletion:^(NSDictionary *result, NSError *error) {
        if (!error) {

            success(true);

        } else {
            NSLog(@"ses kun.. %@",error);
            success(false);
        }
    }];

}
@end