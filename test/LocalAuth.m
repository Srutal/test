//
// Created by srutka on 14/07/16.
// Copyright (c) 2016 srutka. All rights reserved.
//

#import "LocalAuth.h"


@implementation LocalAuth {
    NSString* _email;
    NSString * _password;
}

@synthesize email = _email;
@synthesize password = _password;

- (instancetype)initWithLogin:(NSString *)email password:(NSString *)password {
    self = [super init];
    if (self) {
        self.email = email;
        self.password = password;
    }

    return self;
}

+ (instancetype)authWithLogin:(NSString *)email password:(NSString *)password {
    return [[self alloc] initWithLogin:email password:password];
}

- (BOOL)isSet {
    return _email && _password && _email.length > 0 && _password.length > 0;
}

@end