//
// Created by srutka on 13/07/16.
// Copyright (c) 2016 srutka. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NetworkEngine;

@interface ComLayer : NSObject

@property(nonatomic, strong) NetworkEngine *engine;

- (instancetype)init;

+ (bool)hasAutologinData;

- (void)uploadImage:(NSString *)imageString completion:(void (^)(BOOL))completion;

- (void)doRegistrationWith:(NSString *)email password:(NSString *)password onCompletion:(void (^)(NSString *))completion;

- (void)doLoginWith:(NSString *)email password:(NSString *)password enableAutologin:(bool)enableAutologin onCompletion:(void (^)(BOOL))completion;

//- (void)doLoginWith:(NSString *)email password:(NSString *)password onCompletion:(void (^)(NSString *))completion;
- (void)loginWithStoredAuth:(void (^)(BOOL))completion;


@end