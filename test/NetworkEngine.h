//
// Created by srutka on 12/07/16.
// Copyright (c) 2016 srutka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface NetworkEngine : NSObject

@property(nonatomic, strong) AFHTTPSessionManager *manager;

@property(nonatomic, copy) NSString *url;

+ (NSDictionary *)toJSONRequest:(NSString *)method parametr:(NSDictionary *)param;

- (NSURLSessionDataTask *)sendRequest:(NSDictionary *)request onCompletion:(void (^)(NSDictionary *posts, NSError *error))block;

@end