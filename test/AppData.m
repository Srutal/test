//
// Created by srutka on 13/07/16.
// Copyright (c) 2016 srutka. All rights reserved.
//

#import "AppData.h"
#import "UserInfo.h"

@implementation AppData {

    id _comm;

    UserInfo *_user;

    NSMutableArray *_images;



}

@synthesize comm = _comm;

@synthesize user = _user;

@synthesize images = _images;


- (id)init {
    self = [super init];
    if (self) {

        _images = [@[] mutableCopy];
;
    }
    return self;
}

+ (AppData *)instance {
    static AppData *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}
@end