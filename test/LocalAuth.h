//
// Created by srutka on 14/07/16.
// Copyright (c) 2016 srutka. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LocalAuth : NSObject


@property(nonatomic, copy) NSString *email;
@property(nonatomic, copy) NSString *password;

- (BOOL)isSet;

- (instancetype)initWithLogin:(NSString *)email password:(NSString *)password;

+ (instancetype)authWithLogin:(NSString *)email password:(NSString *)password;
@end