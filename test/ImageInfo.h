//
// Created by srutka on 21/07/16.
// Copyright (c) 2016 srutka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class NSManagedObject;


@interface ImageInfo : NSObject

@property(nonatomic, strong) UIImage *image;
@property(nonatomic, strong) NSManagedObject *manager;

- (instancetype)initWithManager:(NSManagedObject *)manager image:(UIImage *)image;

+ (instancetype)infoWithManager:(NSManagedObject *)manager image:(UIImage *)image;

@end