//
// Created by srutka on 21/07/16.
// Copyright (c) 2016 srutka. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "ImageInfo.h"


@implementation ImageInfo {

@private
    NSManagedObject *_manager;
    UIImage *_image;
}
@synthesize manager = _manager;
@synthesize image = _image;

- (instancetype)initWithManager:(NSManagedObject *)manager image:(UIImage *)image {
    self = [super init];
    if (self) {
        self.manager = manager;
        self.image = image;
    }

    return self;
}

+ (instancetype)infoWithManager:(NSManagedObject *)manager image:(UIImage *)image {
    return [[self alloc] initWithManager:manager image:image];
}

@end