//
// Created by developer on 17/03/16.
//

#import "ContentScreenHandler.h"
#import "AppData.h"
#import "ContentViewController.h"
#import "UIImage+Resize.h"
#import "Reachability.h"
#import "ComLayer.h"
#import "Units.h"
#import "ImageInfo.h"

@implementation ContentScreenHandler {
}


- (void)upload:(ContentViewController *)controller managedObjectContext:(NSManagedObjectContext *)context {

    NSMutableArray *imagesToUpload = [[NSMutableArray alloc] init];

    for (NSManagedObject *images in aData.images) {
        BOOL obj = [[images valueForKey:@"forUpload"] boolValue];
        NSLog(@"%@",obj? @"forUpload false": @"forUpload true");

        if (obj) {

            NSString *imgName = [NSString stringWithFormat:@"%@", [images valueForKey:@"imgname"]];
            NSData *pngData = [NSData dataWithContentsOfFile:[Units imagePathWith:imgName]];
            UIImage *image = [UIImage imageWithData:pngData];

            [imagesToUpload addObject:[ImageInfo infoWithManager:images image:image]];
        }


    }

    if (imagesToUpload == nil || imagesToUpload.count == 0)
        return;

    ImageInfo *imageInfo = imagesToUpload[0];
    // pop first image
    UIImage *image = imageInfo.image;

    UIImage *image2 = [self resizeImage:image];

    NSData *imageData = UIImageJPEGRepresentation(image2, 1.0);
    //NSData *imageData = UIImagePNGRepresentation(image2);

    NSString *imageString = [imageData base64EncodedStringWithOptions:0];

    [aComm uploadImage:imageString completion:^(BOOL success) {
        if(success) {

            [imageInfo.manager setValue:@NO forKey:@"forUpload"];
            // podarilo se odeslat, odebereme
            [imagesToUpload removeObjectAtIndex: 0];
            
            NSLog(@"podarilo se odeslat, odebereme");

            if(imagesToUpload.count == 0) {
                [controller.collectionView reloadData];
                NSLog(@"podarilo se vse odeslat");

            } else {
                [self upload:controller managedObjectContext:context];
            }
        } else {

            NSLog(@"Chyba při odesílání fotografií");
            [controller.collectionView reloadData];

            return;
        }


    }];
}

- (UIImage *)resizeImage:(UIImage *)image {
    CGFloat w = image.size.width;
    CGFloat h = image.size.height;

    CGFloat w1, h1;

    if(w > h) {
        w1 = 800;
        h1 = w1 * h / w;
    } else {
        h1 = 800;
        w1 = h1 * w / h;
    }

    return [image resizedImage: CGSizeMake(w1, h1) interpolationQuality: kCGInterpolationMedium];
}

- (void)updateInterfaceWithReachability:(Reachability *)reachability ref:(ContentViewController *)ref
{

    NetworkStatus netStatus = [reachability currentReachabilityStatus];

    switch (netStatus)
    {
        case NotReachable:        {
            //[ref dismissViewControllerAnimated: true completion: nil];
            ref.connection = false;
            break;
        }

        default: {
            //statusString = NSLocalizedString(@"Internet Access Available", @"");
            ref.connection = true;

            break;
        }
    }
}

@end