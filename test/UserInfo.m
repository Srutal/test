//
// Created by srutka on 14/07/16.
// Copyright (c) 2016 srutka. All rights reserved.
//

#import "UserInfo.h"


@implementation UserInfo {
    NSString *_email;
    NSString *_id;
    NSString *_passwordSalt;
    NSNumber *_superuser;
}
@synthesize superuser = _superuser;
@synthesize passwordSalt = _passwordSalt;
@synthesize id = _id;
@synthesize email = _email;

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];

    if (self) {
        self.email = dictionary[@"email"];
        self.id = dictionary[@"id"];
        self.passwordSalt = dictionary[@"passwordSalt"];
        self.superuser = dictionary[@"superuser"];
    }
    return self;
}


@end