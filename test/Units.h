//
// Created by srutka on 12/07/16.
// Copyright (c) 2016 srutka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@class LocalAuth;

@interface Units : NSObject
+ (NSString *)sha1:(NSString *)string;

+ (BOOL)validateEmail:(NSString *)emailStr;

+ (bool)storeAuth:(LocalAuth *)auth;

+ (LocalAuth *)loadAuth;

+ (NSString *)imagePathWith:(NSString *)imgName;

@end