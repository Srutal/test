//
// Created by srutka on 13/07/16.
// Copyright (c) 2016 srutka. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserInfo;
@class Database;

#define aData [AppData instance]
#define aUser [AppData instance].user
#define aComm [AppData instance].comm

@interface AppData : NSObject

@property(nonatomic, strong) id comm;

@property(nonatomic, strong) UserInfo *user;

@property(nonatomic, strong) NSMutableArray *images;

@property(nonatomic, strong) Database *database;

+ (AppData *)instance;

@end