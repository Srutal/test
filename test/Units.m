//
// Created by srutka on 12/07/16.
// Copyright (c) 2016 srutka. All rights reserved.
//

#import "Units.h"
#import "LocalAuth.h"
#import "JNKeychain.h"

id kSecLogin = @"login";
id kSecPassword = @"password";

@implementation Units {

}

+ (NSString *)sha1:(NSString *)string {

    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];

    CC_SHA1(data.bytes, data.length, digest);

    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];

    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", digest[i]];
    }

    return [output uppercaseString];
}

+ (BOOL)validateEmail:(NSString *)emailStr {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

// ulozeni / smazani auth. udaju
+ (bool)storeAuth:(LocalAuth *) auth {
    if(auth == nil || ![auth isSet]) {
        [JNKeychain deleteValueForKey: kSecLogin];
        [JNKeychain deleteValueForKey: kSecPassword];

        return true;
    } else {
        [JNKeychain saveValue: auth.email forKey: kSecLogin];
        [JNKeychain saveValue: auth.password forKey: kSecPassword];

        return true;
    }

    return false;
}

// acteni auth. udaju
+ (LocalAuth *)loadAuth {

    NSString *email = [JNKeychain loadValueForKey: kSecLogin];
    NSString *password = [JNKeychain loadValueForKey: kSecPassword];

    if(email != nil && password != nil) {
        LocalAuth * ret = [[LocalAuth alloc] init];

        ret.email = email;
        ret.password = password;

        return ret;
    }

    return nil;
}

+ (NSString *)imagePathWith:(NSString *)imgName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = paths[0]; //Get the docs directory
    NSString *imgPath = [documentsPath stringByAppendingPathComponent:imgName];
    return imgPath;
}

@end