//
//  RegistraceViewController.h
//  test
//
//  Created by srutka on 11/07/16.
//  Copyright © 2016 srutka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegistraceScreenHandler.h"
#import "FirstViewController.h"

@interface RegistraceViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *hesloField;
@property (weak, nonatomic) IBOutlet UITextField *repHesloField;
@property (weak, nonatomic) IBOutlet UIButton *regButton;

@property(nonatomic, strong) RegistraceScreenHandler *screenHandler;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

- (IBAction)nextField:(id)nextField;

- (void)showMessage:(NSString *)string;

- (IBAction)doBack:(id)sender;
@end
