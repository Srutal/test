//
//  RegistraceScreenHandler.m
//  test
//
//  Created by srutka on 11/07/16.
//  Copyright © 2016 srutka. All rights reserved.
//

#import "RegistraceScreenHandler.h"
#import "RegistraceViewController.h"
#import "AppData.h"
#import "ComLayer.h"
#import "Units.h"

@implementation RegistraceScreenHandler

- (void)doRegistration:(NSString *)email password:(NSString *)password repPassword:(NSString *)repPassword ref:(RegistraceViewController *)ref {

    if(email == nil || email.length == 0 || password == nil || password.length == 0 || repPassword == nil || repPassword.length == 0) {
        [ref showMessage:@"Prosím zadejte login a heslo"];
        [ref.regButton setEnabled:true];
        return;
    }

    if(![Units validateEmail:email]) {
        [ref showMessage:@"Vloz spravne email"];
        [ref.regButton setEnabled:true];
        return;
    }

    if(password.length > 0 && repPassword.length > 0 && ![password isEqualToString: repPassword]) {
        [ref showMessage:@"Hesla musí být stejná"];
        [ref.regButton setEnabled:true];
        return;
    }

    [ref.activityIndicator startAnimating];

   [aComm doRegistrationWith:email password:password onCompletion:^(NSString * res) {

       [ref showMessage:res];
       [ref.regButton setEnabled:true];
       [ref.activityIndicator stopAnimating];
    }];

}

@end
