//
//  FirstScreenHandler.h
//  test
//
//  Created by srutka on 11/07/16.
//  Copyright © 2016 srutka. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FirstViewController;
@class Reachability;

@interface FirstScreenHandler : NSObject

//- (void)doLogin:(NSString *)email password:(NSString *)password ref:(FirstViewController *)ref;

- (void)doLogin:(NSString *)email password:(NSString *)password enableAutologin:(bool)enableAutologin ref:(FirstViewController *)ref;

- (bool)isLogin:(FirstViewController *)ref;

- (void)tryAutologin:(FirstViewController *)controller;

- (void)updateInterfaceWithReachability:(Reachability *)reachability ref:(FirstViewController *)ref;

@end
