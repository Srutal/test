//
//  RegistraceScreenHandler.h
//  test
//
//  Created by srutka on 11/07/16.
//  Copyright © 2016 srutka. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RegistraceViewController;

@interface RegistraceScreenHandler : NSObject

- (void)doRegistration:(NSString *)email password:(NSString *)password repPassword:(NSString *)repPassword ref:(RegistraceViewController *)ref;

@end
