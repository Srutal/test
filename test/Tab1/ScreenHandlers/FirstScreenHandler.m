//
//  FirstScreenHandler.m
//  test
//
//  Created by srutka on 11/07/16.
//  Copyright © 2016 srutka. All rights reserved.
//

#import "FirstScreenHandler.h"
#import "FirstViewController.h"
#import "AppData.h"
#import "ComLayer.h"
#import "Units.h"
#import "UserInfo.h"
#import "Reachability.h"

@implementation FirstScreenHandler

- (void)doLogin:(NSString *)email password:(NSString *)password enableAutologin:(bool)enableAutologin ref:(FirstViewController *)ref {

    if(email == nil || email.length == 0 || password == nil || password.length == 0) {
        [ref showMessage:@"Prosím zadejte email a heslo"];
        [ref.loginButton setEnabled:true];
        return;
    }

    if(![Units validateEmail:email]) {
        [ref showMessage:@"Vloz spravne email"];
        [ref.loginButton setEnabled:true];

        return;
    }

    [ref.activityIndicator startAnimating];

    [aComm doLoginWith:email password:password enableAutologin:(bool) enableAutologin onCompletion:^(BOOL success)  {

            [self isLogin:ref];
            [ref.activityIndicator stopAnimating];

        }];

}

- (bool)isLogin:(FirstViewController *)ref {
    if (aUser != nil) {
        ref.statusLabel.text = @"Prihlasen";
        ref.emailField.text = aUser.email;
        ref.hesloField.text = @"";
        [ref.loginButton setEnabled:false];

        UIViewController *secondViewController = [ref.storyboard instantiateViewControllerWithIdentifier:@"content"];

        [ref showViewController:secondViewController sender:nil];
        return YES;

    } else {
        [ref.loginButton setEnabled:true];
        return NO;
    }
}

- (void)tryAutologin:(FirstViewController *)ref {
    [ref.activityIndicator startAnimating];
    if ([ComLayer hasAutologinData]) {
        [aComm loginWithStoredAuth:^(BOOL completion) {
            NSLog(@"Try Autologin %@", completion? @"true": @"false");
            [self isLogin:ref];
            [ref.activityIndicator stopAnimating];
        }];
    } else {
        [ref.activityIndicator stopAnimating];
    }

}

- (void)updateInterfaceWithReachability:(Reachability *)reachability ref:(FirstViewController *)ref
{

    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    BOOL connectionRequired = [reachability connectionRequired];
    NSString* statusString = @"";

    switch (netStatus)
    {
        case NotReachable:        {
            statusString = NSLocalizedString(@"Access Not Available", @"Text for access is not available");

            /*
             Minor interface detail- connectionRequired may return YES even when the host is unreachable. We cover that up here...
             */
            connectionRequired = YES;
            break;
        }

        default: {
            //statusString = NSLocalizedString(@"Internet Access Available", @"");
            if (![self isLogin:ref])
                [self tryAutologin:ref];

            break;
        }
    }

    if (connectionRequired)
    {
        NSString *connectionRequiredFormatString = NSLocalizedString(@"%@, Connection Required", @"Concatenation of status string with connection requirement");
        statusString= [NSString stringWithFormat:connectionRequiredFormatString, statusString];
    }
    ref.statusLabel.text = statusString;

}
@end
