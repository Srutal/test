//
//  RegistraceViewController.m
//  test
//
//  Created by srutka on 11/07/16.
//  Copyright © 2016 srutka. All rights reserved.
//

#import "RegistraceViewController.h"
#import "ComLayer.h"
#import "AppData.h"

@interface RegistraceViewController ()

@end

@implementation RegistraceViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _screenHandler = [[RegistraceScreenHandler alloc] init];

    [self.activityIndicator setHidesWhenStopped:true];

    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    singleTap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer: singleTap];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doRegistration:(id)sender {
    [self.view endEditing:true];
    [self.regButton setEnabled:false];
    
    NSString *email = [self.emailField text];
    NSString *heslo = [self.hesloField text];
    NSString *repHeslo = [self.repHesloField text];
    
    [_screenHandler doRegistration:email password:heslo repPassword:repHeslo ref:self];
}

- (IBAction)nextField:(id)nextField {
    UITextField *field = nextField;
    if (field.tag == 1)
        [self.hesloField becomeFirstResponder];

    if (field.tag == 2)
        [self.repHesloField becomeFirstResponder];

    if (field.tag == 3)
        [self doRegistration:nil];
}

- (void)showMessage:(NSString *)string {
    self.statusLabel.text = string;
}

- (void)handleSingleTap:(id)handleSingleTap {
    [self.view endEditing: true];
}

- (IBAction) doBack:(id)sender {
    [self.view endEditing: true];
    [self dismissViewControllerAnimated: true completion: nil];
}
@end
