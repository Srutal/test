//
//  ContentViewController.h
//  test
//
//  Created by srutka on 18/07/16.
//  Copyright © 2016 srutka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "ContentScreenHandler.h"

@interface ContentViewController : UIViewController
@property(nonatomic, strong) id screenHandler;
@property (weak, nonatomic) IBOutlet UIButton *addImgButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;

@property(nonatomic, strong) Reachability *internetReachability;

@property(nonatomic) BOOL deleteMode;

@property(nonatomic) BOOL connection;

- (void)reachabilityChanged:(NSNotification *)note;

- (IBAction)doAdd:(id)sender;

@end
