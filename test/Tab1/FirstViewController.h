//
//  FirstViewController.h
//  test
//
//  Created by srutka on 11/07/16.
//  Copyright © 2016 srutka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FirstScreenHandler.h"

@interface FirstViewController : UIViewController

@property(nonatomic, strong) FirstScreenHandler *screenHandler;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *hesloField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UISwitch *autoLogin;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic) Reachability *internetReachability;

- (IBAction)nextField:(id)nextField;

- (void)showMessage:(NSString *)message;

- (IBAction)doLogin:(id)sender;

@end

