//
//  ContentViewController.m
//  test
//
//  Created by srutka on 18/07/16.
//  Copyright © 2016 srutka. All rights reserved.
//

#import "ContentViewController.h"
#import "CollectionCell.h"
#import "AppData.h"
#import "Reachability.h"
#import "Units.h"

@interface ContentViewController ()

@end

@implementation ContentViewController {
    id _screenHandler;
    BOOL _deleteMode;
    BOOL _connection;
}

@synthesize screenHandler = _screenHandler;

@synthesize deleteMode = _deleteMode;

@synthesize connection = _connection;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _screenHandler = [[ContentScreenHandler alloc] init];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];

    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    [_screenHandler updateInterfaceWithReachability:self.internetReachability ref:self];

    self.flowLayout.itemSize = CGSizeMake(120, 120);
    self.flowLayout.minimumLineSpacing = 10.0;
    self.flowLayout.minimumInteritemSpacing = 5.0;
    self.flowLayout.sectionInset = UIEdgeInsetsMake(10.0, 5.0, 10.0, 5.0);
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    self.collectionView.backgroundColor = [UIColor colorWithWhite:.9 alpha:1.0];

}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    // Fetch the devices from persistent data store
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Images"];
    aData.images = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];

    if (aData != nil && _connection == true){
        [_screenHandler upload:self managedObjectContext: managedObjectContext];
    }else{
        [self.collectionView reloadData];
    }

}


- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [_screenHandler updateInterfaceWithReachability:curReach ref:self];
}

- (IBAction)doDelete:(id)sender {
    UIButton *button = sender;
    if (_deleteMode == false) {
        [self.confirmButton setEnabled:false];
        [self.addImgButton setEnabled:false];
        button.backgroundColor = [UIColor redColor];

        _deleteMode = true;
    } else {
        [self.confirmButton setEnabled:true];
        [self.addImgButton setEnabled:true];
        button.backgroundColor = [UIColor grayColor];

        _deleteMode = false;
    }
}

- (IBAction)doAdd:(id)sender {

    if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        return;

    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    cameraUI.allowsEditing = NO;
    cameraUI.delegate = (id <UINavigationControllerDelegate, UIImagePickerControllerDelegate>) self;

    [self presentViewController: cameraUI animated: YES completion: nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
 
    int time = (int)[[NSDate date] timeIntervalSince1970];
    NSString *timeStamp = [NSString stringWithFormat:@"%d",time];
    
    NSData *pngData = UIImageJPEGRepresentation(image, 1.0);

    NSString *imgName = [NSString stringWithFormat:@"%@.jpg",timeStamp];

    [pngData writeToFile:[Units imagePathWith:imgName] atomically:YES];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new image
    NSManagedObject *newImage = [NSEntityDescription insertNewObjectForEntityForName:@"Images" inManagedObjectContext:context];
    [newImage setValue:timeStamp forKey:@"timestamp"];
    [newImage setValue:imgName forKey:@"imgname"];
    [newImage setValue:@YES forKey:@"forUpload"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
    [picker dismissViewControllerAnimated: true completion: nil];

}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated: true completion: nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [aData.images count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    // Configure the cell...
    NSManagedObject *images = aData.images[indexPath.row];
    NSString *imgName = [NSString stringWithFormat:@"%@", [images valueForKey:@"imgname"]];
    NSData *pngData = [NSData dataWithContentsOfFile:[Units imagePathWith:imgName]];
    UIImage *image = [UIImage imageWithData:pngData];
    [cell.imageDone setHidden:[[images valueForKey:@"forUpload"] boolValue]];
    [cell.imageView setImage:image];

    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObjectContext *context = [self managedObjectContext];
    if (_deleteMode == true){
        //[_screenHandler deletePhoto:indexPath.row ref: self];
        // Delete object from database
        [context deleteObject:aData.images[indexPath.row]];
        
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Can't Delete! %@ %@", error, [error localizedDescription]);
            return;
        }
        
        // Remove device from table view
        [aData.images removeObjectAtIndex:indexPath.row];
        [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
    }
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
   /*
    if (self.fetchingMore == NO && [self.currentResponseObject hasNext] && self.images.count - indexPath.row < 5) {
        self.fetchingMore = YES;

        __weak ContentViewController *weakSelf = self;
        [self.currentResponseObject loadNextWithSuccess:^(PDKResponseObject *responseObject) {
            weakSelf.fetchingMore = NO;
            weakSelf.currentResponseObject = responseObject;
            weakSelf.images = [weakSelf.images arrayByAddingObjectsFromArray:[responseObject pins]];
            [weakSelf.collectionView reloadData];
        } andFailure:nil];
    }
    */
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
