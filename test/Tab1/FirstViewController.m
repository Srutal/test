//
//  FirstViewController.m
//  test
//
//  Created by srutka on 11/07/16.
//  Copyright © 2016 srutka. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import "FirstViewController.h"
#import "AppData.h"
#import "ComLayer.h"
#import "Reachability.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _screenHandler = [[FirstScreenHandler alloc] init];
    [self.activityIndicator setHidesWhenStopped:true];

    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    singleTap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer: singleTap];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];

    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    [_screenHandler updateInterfaceWithReachability:self.internetReachability ref:self];

}

- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [_screenHandler updateInterfaceWithReachability:curReach ref:self];
}

- (IBAction)nextField:(id)nextField {
    UITextField *field = nextField;
    if (field.tag == 1)
        [self.hesloField becomeFirstResponder];

    if (field.tag == 2)
        [self doLogin:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
  //  [_screenHandler isLogin:self];
}

- (void)showMessage:(NSString *)message {
    self.statusLabel.text = message;
}

- (IBAction)doLogin:(id)sender {
    [self.view endEditing:true];
    [self.loginButton setEnabled:false];

    NSString *login = [self.emailField text];
    NSString *password = [self.hesloField text];

    [_screenHandler doLogin:login password:password enableAutologin:self.autoLogin.isOn ref:self];
}

- (void)handleSingleTap:(id)handleSingleTap {
    [self.view endEditing: true];
}

@end
