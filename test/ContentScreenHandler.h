//
// Created by developer on 17/03/16.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ContentViewController;
@class Reachability;

@interface ContentScreenHandler : NSObject

- (void)updateInterfaceWithReachability:(Reachability *)reachability ref:(ContentViewController *)ref;

- (void)upload:(ContentViewController *)controller managedObjectContext:(NSManagedObjectContext *)context;
@end