//
// Created by srutka on 14/07/16.
// Copyright (c) 2016 srutka. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UserInfo : NSObject
@property(nonatomic, strong) NSNumber *superuser;
@property(nonatomic, strong) NSString *passwordSalt;
@property(nonatomic, strong) NSString *id;
@property(nonatomic, copy) NSString *email;

- (id)initWithDictionary:(NSDictionary *)dictionary;
@end